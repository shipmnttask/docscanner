package com.dhuma1981.docscanner.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.dhuma1981.docscanner.R;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 * Created by Dhrumil on 7/17/2017.
 */

public class PDFGenerator extends AsyncTask<Void,Void,String> {

    private static final String TAG = "PDFGenerator";

    private ProgressDialog progressDialog;
    public PDFGeneratorListener pdfGeneratorListener;

    private final Context context;
    private final ArrayList<String> arrayList;


    public PDFGenerator(Context context, ArrayList<String> arrayList, PDFGeneratorListener pdfGeneratorListener){
        this.context = context;
        this.arrayList = arrayList;
        this.pdfGeneratorListener = pdfGeneratorListener;

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.show();
    }

    @Override
    protected String doInBackground(Void... voids) {
        String fileName = "PDF_"+System.currentTimeMillis();
        File parentDirectory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+context.getString(R.string.app_name)+"/");
        if (!parentDirectory.exists()) {
            parentDirectory.mkdir();
        }

        Document document = new Document(PageSize.A4, 0, 0, 0, 0);

        String pdfName = parentDirectory.getAbsolutePath()+"/"+fileName+".pdf";

        if (new File(pdfName).exists()){
            new File(pdfName).delete();
        }

        try {
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(pdfName));
            document.open();

            for (int i = 0; i < arrayList.size(); i++) {
                document.newPage();
//                addBitmapToPDFDocument(arrayList.get(i), document);
                addImageToPDFDocument(arrayList.get(i), document, pdfWriter);
            }
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        document.close();

        return pdfName;
    }

    @Override
    protected void onPostExecute(String pdfFilePath) {
        super.onPostExecute(pdfFilePath);
        if (progressDialog.isShowing()){
            progressDialog.dismiss();
            pdfGeneratorListener.onPDFGenerated(pdfFilePath);
        }
    }

    private void addBitmapToPDFDocument(String imageUrl, Document document) throws IOException, DocumentException {
        Bitmap bitmap = RubberStampUtils.getBitmap(context, BitmapFactory.decodeFile(imageUrl), "Created by "+context.getString(R.string.app_name));
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        Image image = Image.getInstance(stream.toByteArray());

        // image.setBorder(Image.BOX);
        // image.setBorderWidth(10);
        float documentWidth = document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin();
        float documentHeight = document.getPageSize().getHeight() - document.topMargin() - document.bottomMargin();
        image.scaleToFit(documentWidth, documentHeight);

        Log.e(TAG, document.getPageSize().getHeight()+" - "+image.getScaledHeight());

        float leftMargin =  document.getPageSize().getWidth() - image.getScaledWidth();
        float lMargin = leftMargin / 2 ;

        float topMargin =  document.getPageSize().getHeight() - image.getScaledHeight();
        float tMargin = topMargin / 2 ;

        image.setAbsolutePosition(lMargin, tMargin);
        document.add(image);
    }

    private void addImageToPDFDocument(String imageUrl, Document document, PdfWriter pdfWriter) throws IOException, DocumentException {
        Image image = Image.getInstance(imageUrl);

        // image.setBorder(Image.BOX);
        // image.setBorderWidth(10);
        float documentWidth = document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin();
        float documentHeight = document.getPageSize().getHeight() - document.topMargin() - document.bottomMargin();
        image.scaleToFit(documentWidth, documentHeight);

        Log.e(TAG, document.getPageSize().getHeight()+" - "+image.getScaledHeight());

        float leftMargin =  document.getPageSize().getWidth() - image.getScaledWidth();
        float lMargin = leftMargin / 2 ;

        float topMargin =  document.getPageSize().getHeight() - image.getScaledHeight();
        float tMargin = topMargin / 2 ;

        image.setAbsolutePosition(lMargin, tMargin);

        HeaderFooter headerFooter = new HeaderFooter();
        Font font = new Font(Font.FontFamily.UNDEFINED, 10, Font.ITALIC);
        Phrase footer = new Phrase("Created by "+context.getString(R.string.app_name), font);
        headerFooter.setFooter(footer);
        pdfWriter.setPageEvent(headerFooter);

        document.add(image);
    }



    public interface PDFGeneratorListener{
        void onPDFGenerated(String pdfFilePath);
    }
}

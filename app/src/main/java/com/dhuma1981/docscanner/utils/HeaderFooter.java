package com.dhuma1981.docscanner.utils;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Created by Dhrumil on 7/17/2017.
 */

public class HeaderFooter extends PdfPageEventHelper {
    private static final String TAG = "HeaderFooter";

    protected Phrase header = null;
    protected Phrase footer = null;

    public void setHeader(Phrase header) {
        this.header = header;
    }

    public void setFooter(Phrase footer) {
        this.footer = footer;
    }

    public void onStartPage(PdfWriter writer, Document document) {
        if (header != null){
            ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, header, 30, 800, 0);
        }
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        /*ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase("page " + document.getPageNumber()), 550, 30, 0);*/

        if (footer != null){
            ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, footer, 530, 10, 0);
        }
    }
}

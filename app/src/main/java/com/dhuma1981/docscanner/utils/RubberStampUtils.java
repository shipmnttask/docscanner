package com.dhuma1981.docscanner.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;

import com.vinaygaba.rubberstamp.RubberStamp;
import com.vinaygaba.rubberstamp.RubberStampConfig;
import com.vinaygaba.rubberstamp.RubberStampPosition;

/**
 * Created by Dhrumil on 7/17/2017.
 */

public class RubberStampUtils {
    public static Bitmap getBitmap(Context context, Bitmap bitmap, String stampText){
        RubberStampConfig config = new RubberStampConfig.RubberStampConfigBuilder()
                .base(bitmap)
                .rubberStamp(stampText)
                .rubberStampPosition(RubberStampPosition.BOTTOM_RIGHT)
                .alpha(1000)
                .margin(-20, -20)
                .textColor(Color.BLACK)
                /*.textBackgroundColor(Color.TRANSPARENT)*/
                /*.textShadow(0.1f, 5, 5, Color.BLUE)*/
                .textSize(30)
                .textFont("fonts/champagne.ttf").build();

        RubberStamp rubberStamp = new RubberStamp(context);
        return rubberStamp.addStamp(config);
    }

}

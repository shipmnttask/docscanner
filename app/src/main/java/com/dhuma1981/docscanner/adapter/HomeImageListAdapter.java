package com.dhuma1981.docscanner.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dhuma1981.docscanner.ImageTagginActivity;
import com.dhuma1981.docscanner.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Dhrumil on 7/17/2017.
 */

public class HomeImageListAdapter extends RecyclerView.Adapter<HomeImageListAdapter.ViewHolder> {
    private final Context context;
    private ArrayList<String> itemList;

    public HomeImageListAdapter(Context context, ArrayList<String> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_image_single_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String imagePath = itemList.get(position);
        Picasso.with(context).load(new File(imagePath)).into(holder.imageView);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, ImageTagginActivity.class);
                i.putExtra("image",imagePath);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (itemList == null) {
            return 0;
        }
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_single_item_home);
        }
    }
}

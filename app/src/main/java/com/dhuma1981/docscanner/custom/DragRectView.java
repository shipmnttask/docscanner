package com.dhuma1981.docscanner.custom;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.dhuma1981.docscanner.bean.Tag;

import java.util.ArrayList;

/**
 * Created by Dhrumil on 8/9/2017.
 */

public class DragRectView extends View {

    private static final String TAG = "DragRectView";

    private Paint mRectPaint;
    private Paint mLinePaint;

    public static final int DRAW_NOTHING = 0;
    public static final int DRAW_SINGLE_RECT = 1;
    public static final int DRAW_ALL_RECT = 2;

    private static int DRAWING_DEFAULT_METHOD = DRAW_NOTHING;

    private int mStartX = 0;
    private int mStartY = 0;
    private int mEndX = 0;
    private int mEndY = 0;
    private boolean mDrawRect = false;
    private TextPaint mTextPaint = null;

    private Path path = new Path();

    private OnUpCallback mCallback = null;

    private ArrayList<Tag> tagArrayList = new ArrayList<>();

    private int textX, textY;

    private boolean  isDrawText = false;

    private String textData = "";

    public DragRectView(Context context) {
        super(context);
        init();
    }

    public DragRectView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DragRectView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mRectPaint = new Paint();
        mRectPaint.setColor(ContextCompat.getColor(getContext(), android.R.color.white));
        mRectPaint.setStyle(Paint.Style.STROKE);
        mRectPaint.setStrokeWidth(5);

        mTextPaint = new TextPaint();
        mTextPaint.setColor(ContextCompat.getColor(getContext(), android.R.color.white));
        mTextPaint.setTextSize(50);

        mLinePaint = new Paint();
        mLinePaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (DRAWING_DEFAULT_METHOD == DRAW_SINGLE_RECT){
            if (mDrawRect) {
                canvas.drawRect(Math.min(mStartX, mEndX), Math.min(mStartY, mEndY), Math.max(mEndX, mStartX), Math.max(mEndY, mStartY), mRectPaint);
                // canvas.drawText("  (" + Math.abs(mStartX - mEndX) + ", " + Math.abs(mStartY - mEndY) + ")", Math.max(mEndX, mStartX), Math.max(mEndY, mStartY), mTextPaint);
            }
        } else if (DRAWING_DEFAULT_METHOD == DRAW_ALL_RECT){
            if (mDrawRect) {
                if (tagArrayList.size() > 0){
                    for (int i = 0 ; i < tagArrayList.size() ; i++) {
                        Rect taggedRect = new Rect(tagArrayList.get(i).getLeft(),tagArrayList.get(i).getTop(),tagArrayList.get(i).getRight(),tagArrayList.get(i).getBottom());//tagArrayList.get(i).getReactData();
                        canvas.drawRect(taggedRect.left, taggedRect.top, taggedRect.right, taggedRect.bottom, mRectPaint);

                        taggedRect.width();
                        taggedRect.height();
                        if (isDrawText){
                            if (taggedRect.contains(textX, textY)){
                                isDrawText = false;
                                String taggedData = tagArrayList.get(i).getTitle();
                                drawRectText(taggedData, canvas, taggedRect, mTextPaint);
                            }
                        }
                    }
                }
            }
            mDrawRect = false;
        }
    }

    private void drawRectText(String text, Canvas canvas, Rect r, Paint paint) {
        /*paint.setTextSize(50);
        paint.setTextAlign(Paint.Align.CENTER);
        int width = r.width();

        int numOfChars = paint.breakText(text, true, width, null);
        int start = (text.length() - numOfChars) / 2;

        canvas.drawText(text, start, start + numOfChars, r.exactCenterX(), r.exactCenterY()+15, paint);
        //canvas.drawText(text, start, start + numOfChars, r.exactCenterX(), r.exactCenterY()+15, getTextPaint(paint, r.width(), text));*/

         /*     if (text.equals(textData)){
            new AlertDialog.Builder(getContext())
                    .setTitle("Tag!")
                    .setMessage(text)
                    .setCancelable(true)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
        }
        textData = text;*/

        new AlertDialog.Builder(getContext())
                .setTitle("Tag!")
                .setMessage(text)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        // TODO: be aware of multi-touches
        if (DRAWING_DEFAULT_METHOD == DRAW_SINGLE_RECT){
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mDrawRect = false;
                    mStartX = (int) event.getX();
                    mStartY = (int) event.getY();

                    invalidate();
                    break;

                case MotionEvent.ACTION_MOVE:
                    final int x = (int) event.getX();
                    final int y = (int) event.getY();

                    if (!mDrawRect || Math.abs(x - mEndX) > 5 || Math.abs(y - mEndY) > 5) {
                        mEndX = x;
                        mEndY = y;
                        invalidate();
                    }
                    mDrawRect = true;
                    break;

                case MotionEvent.ACTION_UP:
                    if (mCallback != null) {
                        if (DRAWING_DEFAULT_METHOD == DRAW_SINGLE_RECT){
                            mCallback.onRectFinished(new Rect(Math.min(mStartX, mEndX), Math.min(mStartY, mEndY), Math.max(mEndX, mStartX), Math.max(mEndY, mStartX)));
                        }
                    }
                    invalidate();
                    break;

                default:
                    break;
            }
        } else if (DRAWING_DEFAULT_METHOD == DRAW_ALL_RECT){
            if (event.getAction() == MotionEvent.ACTION_UP){
                textX = (int) event.getX();
                textY = (int) event.getY();
                Log.d(TAG, "onTouchEvent: X: "+textX+", Y: "+textY);
                mDrawRect = true;
                isDrawText = true;
                invalidate();
            }
        }
        return true;
    }

    public void eraseRect(){
        mDrawRect = false;
        invalidate();
    }

    public void setDefaultDrawingMethod(int defaultMethod){
        DRAWING_DEFAULT_METHOD = defaultMethod;
        invalidate();
    }

    public void showAllTags(ArrayList<Tag> tagArrayList) {
        this.tagArrayList = tagArrayList;
        mDrawRect = true;
        invalidate();
    }



    public interface OnUpCallback {
        void onRectFinished(Rect rect);
    }

    public void setOnUpCallback(OnUpCallback callback) {
        mCallback = callback;
    }
}

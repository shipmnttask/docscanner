package com.dhuma1981.docscanner.custom;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.dhuma1981.docscanner.R;

/**
 * Created by Dhrumil on 8/9/2017.
 */

public class TextInputDialog {
    public static void showDialog(Context context, final DialogResponseListener dialogResponseListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Title");
        View viewInflated = LayoutInflater.from(context).inflate(R.layout.dialog_text_input_layout, null, false);
        final EditText input =viewInflated.findViewById(R.id.tv_tag_name);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialogResponseListener.onClickDialog(input.getText().toString());
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dialogResponseListener.onClickDialog("");

            }
        });
        builder.show();
    }

    public interface DialogResponseListener {
        void onClickDialog(String result);
    }

}

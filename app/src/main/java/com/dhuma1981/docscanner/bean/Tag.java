package com.dhuma1981.docscanner.bean;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Dhrumil on 8/9/2017.
 */

public class Tag {
    @DatabaseField
    private String title;
    /*@DatabaseField
        private Rect reactData;*/
    @DatabaseField
    private int top;
    @DatabaseField
    private int bottom;
    @DatabaseField
    private int left;
    @DatabaseField
    private int right;
    @DatabaseField
    private String path;

    public Tag() {
        title = "";
        path = "";
    }

    public Tag(int top, int bottom, int left, int right, String title, String path) {
        /*this.reactData = reactData;*/
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
        this.title = title;
        this.path = path;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public String getPath() {
        return path;
    }

    /*public Tag(Rect reactData, String title,String path) {
        *//*this.reactData = reactData;*//*
        this.title = title;
        this.path=path;
    }*/

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /*public Rect getReactData() {
        return reactData;
    }

    public void setReactData(Rect reactData) {
        this.reactData = reactData;
    }*/
}

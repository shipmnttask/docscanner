package com.dhuma1981.docscanner;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.dhuma1981.docscanner.bean.Tag;
import com.dhuma1981.docscanner.custom.DragRectView;
import com.dhuma1981.docscanner.custom.TextInputDialog;
import com.dhuma1981.docscanner.database.DatabaseHelper;
import com.dhuma1981.docscanner.utils.PDFGenerator;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dhrumil on 8/9/2017.
 */

public class ImageTagginActivity extends OrmLiteBaseActivity<DatabaseHelper> implements PDFGenerator.PDFGeneratorListener{

    @BindView(R.id.draw_rect)
    protected DragRectView dragRectView;
    @BindView(R.id.btn_add_tags)
    protected ImageView btnAddTag;
    @BindView(R.id.btn_show_tags)
    protected ImageView btnShowTag;
    @BindView(R.id.btn_generate_pdf)
    protected ImageView btnGeneratePdf;
    @BindView(R.id.image_view)
    protected ImageView ivImage;

    private ArrayList<Tag> tagArrayList;

    private File imageFile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_taggin);

        ButterKnife.bind(ImageTagginActivity.this);

        if(getIntent()!=null && !TextUtils.isEmpty(getIntent().getStringExtra("image"))){
            imageFile=new File(getIntent().getStringExtra("image"));
            Picasso.with(ImageTagginActivity.this).load(imageFile).into(ivImage);
            setTitle(imageFile.getName());
        }else{
            Toast.makeText(this, "Plesae try again!", Toast.LENGTH_SHORT).show();
            finish();
        }

        tagArrayList = new ArrayList<>();

        if (null != dragRectView) {
            dragRectView.setOnUpCallback(new DragRectView.OnUpCallback() {
                @Override
                public void onRectFinished(final Rect rect) {
                    TextInputDialog.showDialog(ImageTagginActivity.this, new TextInputDialog.DialogResponseListener() {
                        @Override
                        public void onClickDialog(String result) {
                            if (result.length() > 0){
                                Toast.makeText(ImageTagginActivity.this, result, Toast.LENGTH_SHORT).show();
                                Tag tag=new Tag(rect.top,rect.bottom,rect.left,rect.right,result,imageFile.getAbsolutePath());
                                getHelper().addTag(tag);
                                tagArrayList.add(tag);
                            }
                            dragRectView.eraseRect();
                        }
                    });
                }
            });
        }
    }

    @OnClick({R.id.btn_add_tags,R.id.btn_show_tags,R.id.btn_generate_pdf})
    void onClicked(ImageView btn){
        switch (btn.getId()){
            case R.id.btn_add_tags:
                btnShowTag.setColorFilter(ContextCompat.getColor(ImageTagginActivity.this, android.R.color.black));
                btnAddTag.setColorFilter(ContextCompat.getColor(ImageTagginActivity.this, android.R.color.white));
                dragRectView.setDefaultDrawingMethod(DragRectView.DRAW_SINGLE_RECT);
                break;
            case R.id.btn_show_tags:
                List<Tag> tags=getHelper().getTags(imageFile.getAbsolutePath());
                if(tags!=null && tags.size()>0){
                    tagArrayList=new ArrayList<>(tags);
                    dragRectView.showAllTags(tagArrayList);
                }else{
                    Toast.makeText(this, "You haven't tagged anything yet!", Toast.LENGTH_SHORT).show();
                    break;
                }

                btnAddTag.setColorFilter(ContextCompat.getColor(ImageTagginActivity.this, android.R.color.black));

                btnShowTag.setColorFilter(ContextCompat.getColor(ImageTagginActivity.this, android.R.color.white));
                dragRectView.setDefaultDrawingMethod(DragRectView.DRAW_ALL_RECT);
                break;
            case R.id.btn_generate_pdf:
                Toast.makeText(this, "Generate PDF", Toast.LENGTH_SHORT).show();
                    ArrayList<String> selectedImageList=new ArrayList<>();
                    selectedImageList.add(imageFile.getAbsolutePath());
                    PDFGenerator pdfGenerator = new PDFGenerator(ImageTagginActivity.this, selectedImageList, ImageTagginActivity.this);
                    pdfGenerator.execute();

                break;
        }
    }

    @Override
    public void onPDFGenerated(final String pdfFilePath) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ImageTagginActivity.this);
        alertBuilder.setMessage("Would you like to open generated PDF?");
        alertBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                File file = new File(pdfFilePath);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });
        alertBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                /*Intent intent = new Intent(GeneratePDFActivity.this, PDFListActivity.class);
                intent.putExtra("parentDirectory", new File(pdfFilePath).getParent());
                startActivity(intent);*/
            }
        });
        alertBuilder.show();
    }
}

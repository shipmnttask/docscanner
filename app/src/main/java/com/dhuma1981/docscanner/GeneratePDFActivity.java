package com.dhuma1981.docscanner;

import android.Manifest;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.dhuma1981.docscanner.adapter.ImageListAdapter;
import com.dhuma1981.docscanner.utils.PDFGenerator;
import com.dhuma1981.docscanner.utils.RealPathUtils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Dhrumil on 7/17/2017.
 */

public class GeneratePDFActivity extends AppCompatActivity implements View.OnClickListener, PDFGenerator.PDFGeneratorListener {

    private static final String TAG = "MainActivity";

    private final int PICK_IMAGE_MULTIPLE = 1;

    private RecyclerView rvSelectedImages;
    private ArrayList<String> selectedImageList = new ArrayList<>();
    private ImageListAdapter imageListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.generate_pdf);

        rvSelectedImages = (RecyclerView) findViewById(R.id.rv_image_list);
        findViewById(R.id.btn_add_images).setOnClickListener(this);
        findViewById(R.id.btn_create_pdf).setOnClickListener(this);

        imageListAdapter = new ImageListAdapter(GeneratePDFActivity.this, selectedImageList);
        rvSelectedImages.setHasFixedSize(true);
        rvSelectedImages.setLayoutManager(new LinearLayoutManager(GeneratePDFActivity.this));
        rvSelectedImages.setAdapter(imageListAdapter);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_add_images){
            if (Build.VERSION.SDK_INT >= 21){
                Dexter.withActivity(GeneratePDFActivity.this).withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        selectImages();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
            } else {
                selectImages();
            }
        } else if (view.getId() == R.id.btn_create_pdf){
            if (selectedImageList.size() > 0){
                PDFGenerator pdfGenerator = new PDFGenerator(GeneratePDFActivity.this, selectedImageList, GeneratePDFActivity.this);
                pdfGenerator.execute();
            } else {
                Toast.makeText(this, "Please select some images.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void selectImages(){
        Intent intent = new Intent();
        intent.setType("image/*");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        }
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && null != data && null != data.getClipData()) {
                ClipData mClipData = data.getClipData();
                selectedImageList.clear();
                for (int i = 0; i < mClipData.getItemCount(); i++) {
                    Uri path = mClipData.getItemAt(i).getUri();
                    String imagePath = RealPathUtils.getPathFromUri(GeneratePDFActivity.this, path);
                    selectedImageList.add(imagePath);
                }
                imageListAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(GeneratePDFActivity.this, "You haven't picked any Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(GeneratePDFActivity.this, "Error: Something went wrong " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onPDFGenerated(final String pdfFilePath) {
        Log.d(TAG, "onPDFGenerated: "+pdfFilePath);

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(GeneratePDFActivity.this);
        alertBuilder.setMessage("Would you like to open generated PDF?");
        alertBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                File file = new File(pdfFilePath);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });
        alertBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                /*Intent intent = new Intent(GeneratePDFActivity.this, PDFListActivity.class);
                intent.putExtra("parentDirectory", new File(pdfFilePath).getParent());
                startActivity(intent);*/
            }
        });
        alertBuilder.show();
    }
}

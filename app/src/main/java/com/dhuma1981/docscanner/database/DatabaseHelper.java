package com.dhuma1981.docscanner.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.dhuma1981.docscanner.bean.Tag;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dhrumil on 8/15/2017.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "DocScanner.db";
    private static final int DATABASE_VERSION = 1;
    private Dao<Tag, Integer> tagDao = null;
    private RuntimeExceptionDao<Tag, Integer> tagRuntimeDao = null;

    private Context context;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onCreate");
            TableUtils.createTable(connectionSource, Tag.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Tag.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        }
    }

    public Dao<Tag, Integer> getTagDao() throws SQLException {
        if (tagDao == null) {
            tagDao = getDao(Tag.class);
        }
        return tagDao;
    }


    public RuntimeExceptionDao<Tag, Integer> getSimpleDataDao() {
        if (tagRuntimeDao == null) {
            tagRuntimeDao = getRuntimeExceptionDao(Tag.class);
        }
        return tagRuntimeDao;
    }

    public int addTag(Tag tag) {
        RuntimeExceptionDao<Tag, Integer> dao = getSimpleDataDao();
        int i = dao.create(tag);
        return i;
    }

    public List<Tag> getTags(String path) {
        DatabaseHelper helper = new DatabaseHelper(context);
        List<Tag> list = null;
        try {
            RuntimeExceptionDao<Tag, Integer> simpleDao = helper.getSimpleDataDao();
            list = simpleDao.queryBuilder().where().eq("path", path).query();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void close() {
        super.close();
        tagDao = null;
        tagRuntimeDao = null;
    }
}

package com.dhuma1981.docscanner;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dhuma1981.docscanner.adapter.HomeImageListAdapter;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dhrumil on 8/15/2017.
 */

public class HomeActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 99;

    @BindView(R.id.rv_home)
    protected RecyclerView rvHome;
    @BindView(R.id.tv_empty_home)
    protected TextView tvEmpty;
    @BindView(R.id.btn_scan_home)
    protected Button btnScan;

    private HomeImageListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(HomeActivity.this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= 21) {
            Dexter.withActivity(HomeActivity.this)
                    .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse response) {
                            showImages();
                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse response) {

                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
        }else{
            showImages();
        }

    }

    private void showImages(){
        ArrayList<String> paths=getImages();
        if(paths!=null) {
            rvHome.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.GONE);
            adapter = new HomeImageListAdapter(HomeActivity.this, getImages());
            rvHome.setHasFixedSize(true);
            rvHome.setLayoutManager(new GridLayoutManager(HomeActivity.this, 3));
            rvHome.setAdapter(adapter);
        }else{
            rvHome.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_scan_home)
    void onScanClicked(){
        int preference = ScanConstants.OPEN_CAMERA;
        if (Build.VERSION.SDK_INT >= 21) {
            final int finalPreference = preference;
            Dexter.withActivity(HomeActivity.this)
                    .withPermission(Manifest.permission.CAMERA)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse response) {
                            startScan(finalPreference);
                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse response) {

                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();

        } else {
            startScan(preference);
        }
    }

    private void startScan(int preference) {
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
        startActivityForResult(intent, REQUEST_CODE);
    }

    private ArrayList<String> getImages(){
        try {
            File path = new File(Environment.getExternalStorageDirectory(), "DocScanner");
            ArrayList<String> paths = new ArrayList<>();
            if (path.exists()) {
                File[] fileNames = path.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File file, String fileName) {
                        return fileName.contains(".png");
                    }
                });
                if(fileNames.length>0) {
                    Arrays.sort(fileNames, new Comparator<File>() {
                        @Override
                        public int compare(File f1, File f2) {
                            return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
                        }
                    });
                    for (int i = 0; i < fileNames.length; i++) {
                        paths.add(fileNames[i].getAbsolutePath());
                    }
                    return paths;
                }else{
                    return null;
                }
            }else{
                return null;
            }

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            showImages();
        }
    }
}

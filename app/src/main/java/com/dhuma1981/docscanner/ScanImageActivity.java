package com.dhuma1981.docscanner;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.dhuma1981.docscanner.utils.PDFGenerator;
import com.dhuma1981.docscanner.utils.RealPathUtils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dhrumil on 8/10/2017.
 */

public class ScanImageActivity extends AppCompatActivity implements PDFGenerator.PDFGeneratorListener {

    private static final int REQUEST_CODE = 99;
    private static final String TAG = "ScanImageActivity";
    @BindView(R.id.btn_camera)
    protected Button btnCamera;
    @BindView(R.id.btn_media)
    protected Button btnMedia;
    @BindView(R.id.btn_add_tags)
    protected Button btnAddTags;
    @BindView(R.id.btn_convert_pdf)
    protected Button btnConvertToPDF;
    @BindView(R.id.iv_scanned_image)
    protected ImageView ivScannedImageView;

    private Uri imageUri;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_scan);
        ButterKnife.bind(ScanImageActivity.this);
    }

    @OnClick({R.id.btn_camera, R.id.btn_media,R.id.btn_add_tags,R.id.btn_convert_pdf})
    void onButtonClicked(Button btn) {
        int preference = 0;
        Intent i;
        switch (btn.getId()) {
            case R.id.btn_camera:
                preference = ScanConstants.OPEN_CAMERA;
                break;
            case R.id.btn_media:
                preference = ScanConstants.OPEN_MEDIA;
                break;
            case R.id.btn_add_tags:
                if(!TextUtils.isEmpty(imageUri.toString())) {
                    i = new Intent(ScanImageActivity.this, ImageTagginActivity.class);
                    i.putExtra("image",imageUri.toString());
                    startActivity(i);
                }else{
                    Toast.makeText(this, "Please select Image for taggin!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_convert_pdf:
                if(!TextUtils.isEmpty(imageUri.getPath())){
                    ArrayList<String> selectedImageList=new ArrayList<>();
                    selectedImageList.add(RealPathUtils.getPathFromUri(ScanImageActivity.this,imageUri));
                    PDFGenerator pdfGenerator = new PDFGenerator(ScanImageActivity.this, selectedImageList, ScanImageActivity.this);
                    pdfGenerator.execute();
                }
                break;
        }
        if (Build.VERSION.SDK_INT >= 21) {
            final int finalPreference = preference;
            Dexter.withActivity(ScanImageActivity.this)
                    .withPermissions(Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                startScan(finalPreference);
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    })
                    .check();
        } else {
            startScan(preference);
        }
    }

    private void startScan(int preference) {
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            imageUri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            Picasso.with(ScanImageActivity.this).load(imageUri).into(ivScannedImageView);
            //getContentResolver().delete(uri, null, null);
            //ivScannedImageView.setImageBitmap(bitmap);
            btnCamera.setVisibility(View.GONE);
            btnMedia.setVisibility(View.GONE);
            btnAddTags.setVisibility(View.VISIBLE);
            btnConvertToPDF.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onPDFGenerated(String pdfFilePath) {
        Log.d(TAG, "onPDFGenerated: "+pdfFilePath);

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ScanImageActivity.this);
        alertBuilder.setMessage("Would you like to open generated PDF?");
        alertBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                File file = new File(imageUri.getPath());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });
        alertBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                /*Intent intent = new Intent(GeneratePDFActivity.this, PDFListActivity.class);
                intent.putExtra("parentDirectory", new File(pdfFilePath).getParent());
                startActivity(intent);*/
            }
        });
        alertBuilder.show();
    }
}
